﻿<?php error_reporting(-1);require_once('../Connections/dbwebhw.php');
require_once('connection.php');ini_set("display_errors", 1); ?>
<?php

require_once("key.php");

if (!isset($_SESSION)) {			
	session_start();
}

if ((isset($_GET['doLogout1'])) &&($_GET['doLogout1']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
 
  if (isset($_COOKIE['remember_me'])) {

		
            unset($_COOKIE['remember_me']);
            unset($_COOKIE['credentials']);
            setcookie('remember_me', null, -1, '/');
            setcookie('credentials', null, -1, '/');
			setcookie('remember_me','', 1);
            setcookie('credentials','', 1);
			
   
        } 
  $_SESSION['MM_Username'] = NULL;
  $_SESSION['MM_UserGroup'] = NULL;
  $_SESSION['PrevUrl'] = NULL;
  unset($_SESSION['MM_Username']);
  unset($_SESSION['MM_UserGroup']);
  unset($_SESSION['PrevUrl']);
  session_destroy();
	

}

if(isset($_COOKIE['remember_me']))
{
	
	list($token, $hmac) = explode(':', $_COOKIE['remember_me'], 2);
	if ($hmac != hash_hmac('md5', $token, $serverkey)) {
		die('tampered token!');
		
	}
	else
	{
	
		if(isset($_COOKIE['credentials']))
		{
		
			$user=readCredentialsCookie();
			
				$STH = $DBH->query(sprintf("SELECT * FROM `user` WHERE userid=%s",
    $user[0] ));
		
			$STH = $DBH->query(sprintf("SELECT * FROM `user` WHERE userid=%s AND password='%s'",	
   $user[0], $user[1]));  

		$STH->setFetchMode(PDO::FETCH_ASSOC);  
		$userId=$user[0];
		$password=$user[1];
		
			 /* retrieve the user content from db
				
			  */

			if($row = $STH->fetch())
			{
				
				if (!isset($_SESSION)) {
 				 session_start();
				}

				  $loginStrGroup = "";
				if (PHP_VERSION >= 5.1) {session_regenerate_id(true);} 
				else {session_regenerate_id();}
				//declare two session variables and assign them
				$_SESSION['MM_Username'] = $row['username'];
				$_SESSION['MM_UserGroup'] = "";	      
			
		
			
					$MM_redirectLoginSuccess = "dashboard/index.php";
				
		
				$randomToken = hash('sha256',uniqid(mt_rand(), true).uniqid(mt_rand(), true));
				$randomToken .= ':'.hash_hmac('md5', $randomToken, $serverkey);
				$STH = $DBH->query(sprintf("UPDATE user SET loginToken='%s' WHERE userid=%s",
			   $randomToken, $userId ));  
				setcookie('remember_me', $randomToken,time()+3600*24*7,'/');	
				generateCredentialsCookie($userId, $password);
				header("Location: " . $MM_redirectLoginSuccess );
				
	
			}
			else
			{
				
				echo "user could not found at db";
			}
			
		}
		else
		{
				echo "credentials is not stored at users pc";				
		}
	}
}


function getTokenForUser($user_id)
{
	global $DBH,$STH;

	$STH = $DBH->query(sprintf("SELECT * FROM `user` WHERE userid=%s",
    $user_id ));
	$STH->setFetchMode(PDO::FETCH_ASSOC); 
			while($row = $STH->fetch())
			{
			
				return $row['accessToken'];
			}	  
	
	
}
function storeTokenForUser($user_id, $token)
{
	
	global $DBH, $STH;
$STH = $DBH->query(sprintf("UPDATE user SET accessToken='%s' WHERE userid= %s ",$token, $user_id ));



	   
		
}
function generateCredentialsCookie($user_id, $password) {
include("key.php");
	$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($secrets), $user_id.':'.$password, MCRYPT_MODE_CBC, md5(md5($secrets))));

    $token = uniqid(mt_rand(), true);

    $encrypted .= ':'.hash_hmac('sha256', $encrypted, $token);

	
    storeTokenForUser($user_id, $token);
	

    setcookie('credentials', $encrypted,time()+3600*24*7,'/');
}

function readCredentialsCookie() {
	include("key.php");
    $parts = explode(':', $_COOKIE['credentials']);
    $token = array_pop($parts);
    $encrypted = implode(':', $parts); //needed incase mcrypt added `:`
	$raw = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($secrets), base64_decode($encrypted), MCRYPT_MODE_CBC, md5(md5($secrets))), "\0");
	
    list ($user_id, $password) = explode(':', $raw, 2);

    if ($token == hash_hmac('sha256', $encrypted,getTokenForUser($user_id))) {

        return array($user_id, $password);
    } else {
        
		$logout="index.php?doLogout1=true";
		header("Location: " . $logout );
		exit;
		return NULL;
    }
}


if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);
  


  

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
?>
<?php
// *** Validate request to login to this site.
if (!isset($_SESSION)) {
  session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];
$registerFormAction= "register/";
if (isset($_GET['accesscheck'])) {
  $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

if (isset($_POST['myusername'])) 
{
  $loginUsername=$_POST['myusername'];
  $password=$_POST['mypassword'];
  $MM_fldUserAuthorization = "";
  $MM_redirectLoginSuccess = "dashboard/index.php";
  $MM_redirectLoginFailed = "index.php";
  $MM_redirecttoReferrer = false;
  mysql_select_db($database_dbwebhw, $dbwebhw);
  
  $LoginRS__query=sprintf("SELECT username, password, userid FROM `user` WHERE username=%s AND password=%s",
    GetSQLValueString($loginUsername, "text"), GetSQLValueString($password, "text")); 
   
  $LoginRS = mysql_query($LoginRS__query, $dbwebhw) or die(mysql_error());
  $loginFoundUser = mysql_num_rows($LoginRS);
  while ($row = mysql_fetch_assoc($LoginRS)) 
  {
    $userId=$row['userid'];

   }

  if ($loginFoundUser) 
  {
     $loginStrGroup = "";
	if (PHP_VERSION >= 5.1) {session_regenerate_id(true);} 
	else {session_regenerate_id();}
    //declare two session variables and assign them
    $_SESSION['MM_Username'] = $loginUsername;
    $_SESSION['MM_UserGroup'] = $loginStrGroup;	      
    if (isset($_SESSION['PrevUrl']) && false) 
	{
      $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];	
    }
	$randomToken = hash('sha256',uniqid(mt_rand(), true).uniqid(mt_rand(), true));
	$randomToken .= ':'.hash_hmac('md5', $randomToken, $serverkey);
	$STH = $DBH->query(sprintf("UPDATE user
SET loginToken=%s
 WHERE userid=%s",
   GetSQLValueString($randomToken, "text"), GetSQLValueString($userId, "text") ));  
	setcookie('remember_me', $randomToken,time()+3600*24*7,'/');	
	generateCredentialsCookie($userId, $password);
    header("Location: " . $MM_redirectLoginSuccess );
  }
  else 
  {

    header("Location: ". $MM_redirectLoginFailed );
  }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome!</title>
</head>

<body>
<table width="300" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#CCCCCC">
<tr>
<form name="form1" method="POST" action="<?php echo $loginFormAction; ?>">
<td>
<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#FFFFFF">
<tr>
<td colspan="3"><strong>Member Login </strong></td>
</tr>
<tr>
<td width="78">Username</td>
<td width="6">:</td>
<td width="294"><input name="myusername" type="text" id="myusername"></td>
</tr>
<tr>
<td>Password</td>
<td>:</td>
<td><input name="mypassword" type="password" id="mypassword"></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><input type="submit" name="Submit" value="Login"></td>
</tr>
</table>
</td>
</form>
</tr>
<tr>
<form name="form2" method="POST" action="<?php echo $registerFormAction; ?>">
<td>
<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#FFFFFF">
<tr>
<td colspan="3"><strong>Do not have an account? <input type="submit" name="Submit" value="Register">  </strong></td>
</tr>

</table>
</td>
</form>

</tr>
</table>
</body>
</html>