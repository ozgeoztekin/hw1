USE s2602350;
CREATE TABLE question
(
  q_number Int NOT NULL AUTO_INCREMENT,
  q_text Text NOT NULL,
  PRIMARY KEY (q_number)
)
;



CREATE TABLE choice
(
  c_text Text NOT NULL,
  correct Bool NOT NULL,
  q_number Int NOT NULL,
  c_number Int NOT NULL,
   CONSTRAINT question_to_choice_u 
      UNIQUE(q_number,c_number) ,
  CONSTRAINT question_to_choice_fk FOREIGN KEY (q_number) REFERENCES question(q_number)
)
;

CREATE TABLE `quizattempts` (
  `attemptid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `successrate` int(11) NOT NULL,
  `completeddate` date NOT NULL,
  PRIMARY KEY (`attemptid`)
)  ;

CREATE TABLE `user` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `accessToken` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loginToken` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `userid` (`userid`)
)  ;

ALTER TABLE `choice`
  ADD CONSTRAINT `question_to_choice_fk` FOREIGN KEY (`q_number`) REFERENCES `question` (`q_number`);



