
INSERT INTO `question`(`q_number`, `q_text`) VALUES (NULL,"What is the capital of Netherlands?" );
INSERT INTO `question`(`q_number`, `q_text`) VALUES (NULL,"How many sense organ does human have?" );
INSERT INTO `question`(`q_number`, `q_text`) VALUES (NULL,"Which is the year of Ataturk's birth?" );

INSERT INTO `choice`(`c_text`, `correct`, `q_number`, `c_number`) VALUES ("Istanbul",0,1,0);
INSERT INTO `choice`(`c_text`, `correct`, `q_number`, `c_number`) VALUES ("Amsterdam",1,1,1);
INSERT INTO `choice`(`c_text`, `correct`, `q_number`, `c_number`) VALUES ("Groningen",0,1,2);
INSERT INTO `choice`(`c_text`, `correct`, `q_number`, `c_number`) VALUES ("Beysukent",0,1,3);
INSERT INTO `choice`(`c_text`, `correct`, `q_number`, `c_number`) VALUES ("Izmir",0,1,4);
INSERT INTO `choice`(`c_text`, `correct`, `q_number`, `c_number`) VALUES ("0",0,2,0);
INSERT INTO `choice`(`c_text`, `correct`, `q_number`, `c_number`) VALUES ("1",0,2,1);
INSERT INTO `choice`(`c_text`, `correct`, `q_number`, `c_number`) VALUES ("3",0,2,2);
INSERT INTO `choice`(`c_text`, `correct`, `q_number`, `c_number`) VALUES ("5",1,2,3);
INSERT INTO `choice`(`c_text`, `correct`, `q_number`, `c_number`) VALUES ("1845",0,3,0);
INSERT INTO `choice`(`c_text`, `correct`, `q_number`, `c_number`) VALUES ("1879",0,3,1);
INSERT INTO `choice`(`c_text`, `correct`, `q_number`, `c_number`) VALUES ("1881",1,3,2);
INSERT INTO `choice`(`c_text`, `correct`, `q_number`, `c_number`) VALUES ("1889",0,3,3);

INSERT INTO `quizattempts` (`attemptid`, `userid`, `successrate`, `completeddate`) VALUES
(1, 2, 3, '2014-01-17'),
(2, 2, 1, '2014-01-18'),
(3, 8, 3, '1970-01-01'),
(4, 8, 3, '2014-01-23'),
(5, 8, 0, '2014-01-23'),
(6, 8, 2, '2014-01-24'),
(7, 13, 3, '2014-01-24');

INSERT INTO `user` (`userid`, `username`, `password`, `accessToken`, `loginToken`) VALUES
(1, 'test', 'test', '49244554252b9f3e590a0f5.13542776', '2e177398fac9eac6397b57c527e284ddac72220b1d699f61e70a5a9861c108f8:a224cd6440d331950d47dd9840430963'),
(2, 'tost', 'tost', '29980135452b64222acbb75.16583674', '75b7cd2860b975b19d2e654071538a4d1837ddc67b7818414487885b159c59ad:df35fda4d2c3a1dc511ef05aa0092379'),
(5, 'ozge', 'ozge', '171083758552b9f3d8b422a7.48052665', 'e0a4eba4e7bfe3717e22a6ed136af2c638d49169909e6c6099be1cc4baeb1364:4313278b6721039e3339f15f84f126d3'),
(6, 'ggg', 'ggg', '82522784852dac312301541.73856425', '5ae6025b8df0e042ee89c1d39ffb0638df20f5a27f41c1ea98f4a25e2bed50e1:d35ef4a7a58d925be831683627d099e6'),
(7, 'sss', 'sss', '212669230452dac6c2d8b871.78870782', '397e74fad8ce1616a99d10f26e844d396d50ed65751238942c1482f33203dde8:d18ff341e8077e81571d7d6617ae943a'),
(8, 'a', 'a', '140424419852e1a5c86b09f9.86490494', 'cfb99d04aec9572c735a7388ec2df99cadee0ca9dd25a75101cffcbf938a5d24:01a714a6a52750da2e8840870936a1b9'),
(9, 'b', 'b', '39244007552dacde8784be4.05314465', '4d296f92b0fbcdb07aea4240b54106a369181f7bcf5d99d9802a256cb98dfaad:af259495d824200963f8f48082f745e4'),
(11, 'f', 'f', '33979454852e1866fa89e61.94750284', '355349cdca24dae97c457505c5a90613d0cef8fb38d9c142e752b691f1912f27:9ea167d80c0517c0b12158d18efff455'),
(13, 'ozgee', 'ozgee', '59345877552e1a929163bb3.93971178', 'ba8aee154cc0e5ef8cdfaab7ea3d90c336192c8ec63df751698ccc6d10bcf35f:607e9371fac82ec630eeb2d05e4b2c49');
